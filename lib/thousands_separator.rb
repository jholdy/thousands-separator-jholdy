class ThousandsSeparator
    def format(number_or_string, seperator=',')
        unless number_or_string.is_a?(Fixnum) || number_or_string.is_a?(String)
            return "0"
        end
        if number_or_string.to_s.length > 15 || number_or_string.to_s.length < 1
            return "0"
        end
        array_number = number_or_string.to_i.to_s.reverse.split("")
        tam          = number_or_string.to_i.to_s.length - 1
        array_format = []
      
        for count in 0..tam do
            if ((count%3==0)&&(count>0))
                array_format << "#{seperator}"                     
            end
            array_format << array_number[count]     
        end

        array_format.join.reverse
    end
end