require 'thousands_separator'
describe ThousandsSeparator do
  context '#format' do
    
    it 'when invalid parameter in type' do
      format = ThousandsSeparator.new
      expect(format.format(1.234)).to eq("0")
    end
    
    it 'when an invalid parameter is larger than 15' do
      format = ThousandsSeparator.new
      expect(format.format("1234567890123456")).to eq("0")
    end

    it 'when invalid parameter is less than 1' do
      format = ThousandsSeparator.new
      expect(format.format("")).to eq("0")
    end

    it 'When the parameter is valid and even, relative to the string size, number 123456' do
      format = ThousandsSeparator.new
      expect(format.format(123456)).to eq("123,456")
    end

    it 'When the parameter is odd, relative to the size of the string, number 123' do
      format = ThousandsSeparator.new
      expect(format.format(123)).to eq("123")
    end

    it 'When valid parameter is odd, relative to string size, number 12345' do
      format = ThousandsSeparator.new
      expect(format.format('12345')).to eq("12,345")
    end

    it 'when the parameter is valid and the string is with the minus sign' do
      format = ThousandsSeparator.new
      expect(format.format('-12345')).to eq("-12,345")
    end

    it 'When the parameter for valid and the number is negative' do
      format = ThousandsSeparator.new
      expect(format.format(-12345)).to eq("-12,345")
    end

    it 'when the parameter is valid and use a separator, with a string' do
      format = ThousandsSeparator.new
      expect(format.format('-12345','.')).to eq("-12.345")
    end

    it 'When the parameter is valid and using a separator, with a number' do
      format = ThousandsSeparator.new
      expect(format.format(-12345,'.')).to eq("-12.345")
    end
    
  end
end